package facci.aragundibryan.carro;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolder> {

   List<Carro> listado;

    public Adaptador(List<Carro> listado) {
        this.listado = listado;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout, null, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        holder.id.setText(listado.get(position).getId().toString());
        holder.nombre.setText(listado.get(position).getNombre().toString());
        holder.modelo.setText(listado.get(position).getModelo().toString());
        holder.color.setText(listado.get(position).getColor().toString());
        holder.precio.setText(listado.get(position).getPrecio().toString());
        Picasso.get().load(listado.get(position).getFoto()).into(holder.foto);

        holder.eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Carro carro = Carro.findById(Carro.class, listado.get(position).getId());
                carro.delete();



                Log.i("delete", "se elimina el registro" + listado.get(position).getId());



            }
        });


        holder.editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Carro carro = Carro.findById(Carro.class, listado.get(position).getId());
                carro.setNombre(holder.nombre.getText().toString());
                carro.setModelo(holder.modelo.getText().toString());
                carro.setColor(holder.color.getText().toString());
                carro.setPrecio(Integer.valueOf(holder.precio.getText().toString()));
                carro.save();

                Log.i("editar", listado.get(position).getNombre()+ " " +
                        ""+ listado.get(position).getModelo()+ ""+
                        ""+ listado.get(position).getColor()+ ""+
                        ""+ listado.get(position).getPrecio()+
                        " se  cambio por" +
                        holder.nombre.getText().toString() + ""
                        + holder.modelo.getText().toString()+ ""
                        + holder.color.getText().toString()+ ""
                        + holder.precio.getText().toString()
                        );



            }
        });

    }

    @Override
    public int getItemCount() {
        return listado.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        ImageView foto;
        EditText id, nombre, modelo, color, precio;
        Button editar, eliminar;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            foto = itemView.findViewById(R.id.foto);
            id = itemView.findViewById(R.id.id);
            nombre = itemView.findViewById(R.id.nombre);
            modelo = itemView.findViewById(R.id.modelo);
            color = itemView.findViewById(R.id.color);
            precio = itemView.findViewById(R.id.precio);
            editar = itemView.findViewById(R.id.editar);
            eliminar = itemView.findViewById(R.id.eliminar);






        }
    }
}
