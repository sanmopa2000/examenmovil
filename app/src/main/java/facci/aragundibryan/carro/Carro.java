package facci.aragundibryan.carro;

import com.orm.SugarRecord;

public class Carro extends SugarRecord<Carro> {

    String nombre;
    String modelo;
    String color;
    Integer precio;
    String foto;

    public Carro() {
    }

    public Carro(String nombre, String modelo, String color, Integer precio, String foto) {
        this.nombre = nombre;
        this.modelo = modelo;
        this.color = color;
        this.precio = precio;
        this.foto = foto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }



}
