package facci.aragundibryan.carro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class Admin extends AppCompatActivity {

    RecyclerView listado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);


        listado = findViewById(R.id.listado);

        List<Carro> carros = Carro.listAll(Carro.class);

        carros.forEach((item)-> Log.e("Test", item.getNombre() ));


        listado.setLayoutManager(new LinearLayoutManager(this));


        Adaptador adaptador = new Adaptador(carros);
        listado.setAdapter(adaptador);


    }
}