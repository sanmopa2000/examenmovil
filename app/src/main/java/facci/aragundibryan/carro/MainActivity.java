package facci.aragundibryan.carro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    EditText nombre, modelo, color, precio, foto;
    Button guardar, listar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        nombre= findViewById(R.id.nombre);
        modelo = findViewById(R.id.modelo);
        color = findViewById(R.id.color);
        precio = findViewById(R.id.precio);
        foto = findViewById(R.id.foto);

        guardar = findViewById(R.id.guardar);
        listar = findViewById(R.id.listar);


        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Carro carro = new Carro(
                        nombre.getText().toString(),
                        modelo.getText().toString(),
                        color.getText().toString(),
                        Integer.parseInt(precio.getText().toString()),
                        foto.getText().toString()
                );

                Toast.makeText(MainActivity.this, "Carro guardado", Toast.LENGTH_LONG).show();
                carro.save();
                limpiar();





                Log.i("test", "Se creo el registro");

            }
            private void limpiar(){
                nombre.setText("");
                modelo.setText("");
                color.setText("");
                precio.setText("");
                foto.setText("");
            }

        });



        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Admin.class );
                startActivity(intent);

            }
        });



    }
}